var express = require('express')
var cors = require('cors')
const mysql = require('mysql2');
const PORT = process.env.PORT || 5000
const connection = mysql.createConnection({
host: '157.245.59.56',
port: '3366',
user: '6300472',
password: '6300472',
database: '6300472'
});
var app = express()
app.use(cors())
app.use(express.json())
app.get('/train', function (req, res, next) {
connection.query(
'SELECT * FROM `train`',
function(err, results, fields) {
res.json(results);
}
);
})
app.get('/train/:id', function (req, res, next) {
const id = req.params.id;
connection.query(
'SELECT * FROM `train` WHERE `id` = ?',
[id],
function(err, results) {
res.json(results);
}
);
})
app.post('/train', function (req, res, next) {
connection.query(
'INSERT INTO `train`(`origin`, `departure`, `destination`, `arrival`, `remark`) VALUES (?, ?, ?, ?, ?)',
[req.body.ต้นทาง, req.body.ออก, req.body.ปลายทาง, req.body.ถึง, req.body.remark],
function(err, results) {
res.json(results);
}
);
})
app.put('/train', function (req, res, next) {
connection.query(
'UPDATE `train` SET `origin`= ?, `departure`= ?, `destination`= ?, `arrival`= ?, `remark`= ? WHERE id = ?',
[req.body.ต้นทาง, req.body.ออก, req.body.ปลายทาง, req.body.ถึง, req.body.หมายเหตุ, req.body.id],
function(err, results) {
res.json(results);
}
);
})
app.delete('/train', function (req, res, next) {
connection.query(
'DELETE FROM `train` WHERE id = ?',
[req.body.id],
function(err, results) {
res.json(results);
}
);
})
app.listen(PORT, function () {
console.log('CORS-enabled web server listening on port '+PORT)
})